'use strict';
const assert = require('assert');
const runsv = require('runsv');

const genServer = require('..');

// timers/promises is not supported in node < v16
function immediate(result){
	return new Promise((resolve) => {
		resolve(result);
	});
}

const REDIS = 'redis://localhost:6379';
function createRedisMock(){
	let mock = {
		calls: [],
		createClient(options){
			mock.calls.push(['createClient', options]);
			return {
				type: 'redis-client',
				async connect(){
					mock.calls.push(['connect']);
					return await immediate('ok');
				},
				async quit(){
					mock.calls.push(['quit']);
					return await immediate('ok');
				},
				async set(key, value){
					mock.calls.push(['set', key, value]);
					return await immediate(value);
				},
				once(event, callback){
					setImmediate(() => callback());
				}
			};
		}
	};
	return mock;
}
function createService(options){
	const driver = createRedisMock();
	return [genServer.createAsyncService({
		name:'redis',
		driver,
		async start(deps, redis, options){
			const client = redis.createClient(options);
			await client.connect();
			return client;
		},
		async stop (redis){
			await redis.quit();
		}

	})(options), driver];
}
describe('Promise interface',function(){
	describe('Happy path', function(){
		before(async function(){
			const [service, mock] = createService(REDIS);
			this.mock = mock;
			this.service = service;
			const sv = runsv.create().async();
			sv.addService(service);
			await sv.start();
			const {redis} = sv.getClients();
			this.client = redis;
			await redis.set('k', 1);
			await sv.stop();
		});
		it('service must have a name', function(){
			const {service} = this;
			assert.deepStrictEqual(service.name, 'redis');
		});
		it('#start() should start the client with provided options', function(){
			const {mock} = this;
			const [createClient, [connect]] = mock.calls;
			assert.deepStrictEqual(createClient[0], 'createClient');
			assert.deepStrictEqual(createClient[1], REDIS);
			assert.deepStrictEqual(connect, 'connect');
		});
		it('#getClient() should return the client', function(){
			const {mock} = this;
			const [,,set] = mock.calls;
			assert.deepStrictEqual(set[0], 'set');
		});
		it('#stop() should quit and remove the client', async function(){
			const {service, mock } = this;
			const [,,, [quit]] = mock.calls;
			assert.deepStrictEqual(quit, 'quit', 'quit not called');
			assert.deepStrictEqual(null, service.getClient());
			// Starting the service again will fail if client was not set to null
			await service.start();
		});
	});
	describe('Prevent double start', function(){
		it('should throw an exception if started more than once', async function(){
			const [service, mock] = createService(REDIS);
			this.mock = mock;
			this.service = service;
			// First time should work
			await service.start();
			try{
				// second time should fail
				await service.start();
			}catch(e){
				assert.deepStrictEqual(e.message, 'already started: redis');
				return;
			}
			throw new Error('double start did not throw an error');
		});
	});
	describe('When stopped before started', function(){
		it('should pass',async function(){
			const [service] = createService();
			await service.stop();
		});
	});
	describe('#start(deps, driver, options) not creating a client', function(){
		it('should fail', async function(){
			let driver = createRedisMock();
			let service = genServer.createAsyncService({
				name:'redis',
				driver,
				async start(){
					return await immediate();
				},
				async stop (){
					throw new Error('should not happen');
				}
			})(REDIS);
			try{
				await service.start();
			}catch(e){
				assert.deepStrictEqual(e.message, 'no client');
				return;
			}
			throw new Error('unexpected code branch');
		});
	});
	describe('#create(options)', function(){
		it('should be able to set up name', function(){
			let driver = createRedisMock();
			let service = genServer.createAsyncService({
				name:'first',
				driver,
				async start(){
					return await immediate();
				},
				async stop (){
					throw new Error('should not happen');
				}
			})({name: 'second'});
			assert.deepStrictEqual(service.name, 'second');
		});
		it('should be able to set up driver', async function(){
			let driver = createRedisMock();
			let service = genServer.createAsyncService({
				name:'first',
				async start(deps, driver, options){
					const client = driver.createClient(options);
					await client.connect();
					return client;
				},
				async stop (){
					throw new Error('should not happen');
				}
			})({name: 'second', driver});
			await service.start();
			const client = service.getClient();
			assert.deepStrictEqual(client.type, 'redis-client');
		});
	});
});
