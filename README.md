# runsv gen-service

A generic [runsvjs](https://gitlab.com/runsvjs/runsv) service wrapper

[![pipeline status](https://gitlab.com/runsvjs/gen-server/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/gen-server/-/commits/master)
[![coverage report](https://gitlab.com/runsvjs/gen-server/badges/master/coverage.svg)](https://gitlab.com/runsvjs/gen-server/-/commits/master)

## Install

```bash
$ npm install runsv-gen-server
```

## Usage

### Async/Promise clients
`#createAsyncService(setup)`  

#### Params

`#setup`
* `name` Default service name  
* `driver` Options default module i.e pg, redis  
* `start(deps, driver, options)` A function that returns a (ready to work) client  
	* `#deps` Dependencies injected by runsv, if any  
	* `#driver` The driver module   
	* `#options` Options provided for a concrete service instance  
* `stop(client)` A function that disconnect/terminate/close the client  
	* `#client` An instance of the client to be terminated  

Returns a `create(options)` function that creates an instance of the service.  

`#options`  
* `name` Optional service name. Overrides default name.  
* `driver` Optional driver. Overrides default driver.  

```javascript
const runsv = require('runsv');
const {createAsyncService} = require('runsv-gen-service');

const db = require('some-db');
// create your own service
const createDbService = createAsyncService({
	name: 'db',
	driver: db,
	start: async function(deps, db, options){
		const client = await db.create(options);
		await client.connect();
		return client;
	},
	stop: async function(dbClient){
		await dbClient.disconnect();
	}
});

//...

const sv = runsv.create();
const myDbService = createDbService({user: 'root', password: 'root'});
sv.addService(myDbService);
await sv.start();
```

### Callback clients
`#createCallbackService(setup)`  

#### Params

`#setup`
* `name` Default service name  
* `driver` Options default module i.e pg, redis  
* `start(deps, driver, options, callback)` A function that returns a (ready to work) client  
	* `#deps` Dependencies injected by runsv, if any  
	* `#driver` The driver module  
	* `#options` Options provided for a concrete service instance  
	* `#callback(err, client)` Callback to return the client  
* `stop(client, callback)` A function that disconnect/terminate/close the client  
	* `#client` An instance of the client to be terminated  
	* `#callback(err)` Callback to notify that we are done with the client  

Returns a `create(options)` function that creates an instance of the service.

`#options`  
* `name` Optional service name. Overrides default name.  
* `driver` Optional driver. Overrides default driver.  

```javascript
const runsv = require('runsv');
const {createCallbackService} = require('runsv-gen-service');

const db = require('some-db');
const setup = {
	name:'db', // default name
	driver, // default driver
	start(deps, db, options, callback){
		const client = db.createClient(options);
		client.connect();
		return callback(null, client);
	},
	stop (dbClient, callback){
		dbClient.disconnect(callback);
	}
};
const createDbService = createCallbackService(setup);
const sv = runsv.create();
const myDbService = createDbService({user: 'root', password: 'root'});
sv.addService(myDbService);
sv.start(err, callback);
```
